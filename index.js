//B1: Import thư viện express
//import express from express
const express = require('express');

const mongoose = require('mongoose');

//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;

const courseRouter = require("./app/routes/course.router");
const reviewRouter = require("./app/routes/review.router");

// Khai báo tạm model tại đây
const reviewSchema = require("./app/models/review.model");
const courseSchema = require("./app/models/course.model");

//Cấu hình để sử dụng json
app.use(express.json());

// Middleware
// Middleware console log ra thời gian hiện tại
app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
});

// Middleware console log ra request method
app.use((req, res, next) => {
    console.log("Request method:", req.method);

    next();
})

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/R46_Course_Review")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });

//Khai báo các api
app.get('/', (req, res) => {
    let today = new Date();
    let message = `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`;
    //res.send(message);
    res.status(200).json({
        message
    })
})

//khai báo phương thức get
app.get('/get-method', (req, res) => {
    console.log('Get - Method');
    res.json({
        message : "Get - Method"
    })
})

//khai báo phương thức post
app.post('/post-method', (req, res) => {
    console.log('Post - Method');
    res.json({
        message : "Post - Method"
    })
})

//khai báo phương thức put
app.put('/put-method', (req, res) => {
    console.log('Put - Method');
    res.json({
        message : "Put - Method"
    })
})

//khai báo phương thức delete
app.delete('/delete-method', (req, res) => {
    console.log('Delete - Method');
    res.json({
        message : "Delete - Method"
    })
})

//khai bao get param
app.get('/get-data/:para1/:param2', (req, res) => {
    let param1 = req.params.para1;
    let param2 = req.params.param2;
    res.json({
        param1,
        param2
    })
})

//khai bao get query string
app.get('/query-data/:id', (req, res) => {
    let id = req.params.id;
    let query = req.query;
    let name = query.name
    res.json({
        id,
        name,
        query
    })
})

//khai báo request body
app.post('/update-data', (req, res) => {
    let body = req.body;
    res.json({
        body
    })
})

// Sử dụng router 
app.use("/api/v1/courses", courseRouter);
app.use("/api/v1/reviews", reviewRouter);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
