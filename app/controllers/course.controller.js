//import model
const courseModel=require("../models/course.model");

const createCourse=async (req, res) => {
    //B1 thu thap du lieu
    const {
        title,
        description,
        noStudent
    } = req.body;
    console.log('');
    //B2 validate du lieu
    if(!title)
    {
        return res.status(400).json({
            message:"Title khong hop le"
        })
    }

    if(noStudent < 0)
    {
        return res.status(400).json({
            message:"So hoc sinh khong hop le"
        })
    }

 console.log(title);
 console.log(description);
    try {
           // B3 xu ly du lieu

           var newCourse={
            title,
            description,
            noStudent
           }

           const result= await courseModel.create(newCourse);
           return res.status(200).json({
            message:"Tao course thanh cong",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }

}
const getAllCourse=async (req, res) => {
      //B1 thu thap du lieu
       //B2 validate du lieu
         // B3 xu ly du lieu
         try {
            const result=await courseModel.find();
            return res.status(200).json({
                message:"Lay danh sach course thanh cong",
                data: result
            })
         } catch (error) {
            return res.status(500).json({
                message:"Co loi xay ra"
            })
         }
}

const getCourseById=(req, res) => {
    const courseid = req.params.courseid;
    res.json({
        message: "GET courses by Id=" + courseid
    })
}

const updateCourseById=(req, res) => {
    const courseid = req.params.courseid;

    res.json({
        message: "PUT course id = " + courseid
    })
}

const deleteCourseById=(req, res) => {
    const courseid = req.params.courseid;

    res.json({
        message: "DELETE course id = " + courseid
    })
}
module.exports={getAllCourse , createCourse, getCourseById,updateCourseById,deleteCourseById};