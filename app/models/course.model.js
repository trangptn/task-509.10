const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        // unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    // Nếu 1 course có 1 review 
    // reviews: 
    //     {
    //         type: mongoose.Types.ObjectId,
    //         ref: "Review"
    //     }
    
    // Nếu 1 course có nhiều review 
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Review"
        }
    ]
}, {
    timestamps: true
});

module.exports = mongoose.model("Course", courseSchema);