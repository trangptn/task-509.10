const express = require("express");

const router = express.Router();

const courseController=require("../controllers/course.controller");

const {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getCourseByIDMiddleware,
    deleteCourseMiddleware,
    updateCourseMiddleware
} = require("../middlewares/course.middleware");
const { getAllCourse } = require("../controllers/course.controller");

router.get("/", getAllCourseMiddleware, courseController.getAllCourse);

router.post("/", createCourseMiddleware,courseController.createCourse );

router.get("/:courseid", getCourseByIDMiddleware,courseController.getCourseById );

router.put("/:courseid", updateCourseMiddleware,courseController.updateCourseById );

router.delete("/:courseid", deleteCourseMiddleware,courseController.deleteCourseById);

module.exports = router;